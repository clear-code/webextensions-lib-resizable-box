/*
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/
'use strict';

export const TYPE_RESIZED = 'resizable-box-resized';
const MIN_SIZE = '1em';

let mSizes;

let mResizableBoxCount = 0;

const mResizableBoxes = new Map();

export function init(sizes) {
  mSizes = (sizes && typeof sizes == 'object') ? JSON.parse(JSON.stringify(sizes)) : {};

  const splitters = new Set();
  for (const splitter of document.querySelectorAll('.splitter')) {
    const previousBox = findPreviousResizableBox(splitter);
    const nextBox = findNextResizableBox(splitter);

    if (!previousBox ||
        !nextBox)
      continue;

    splitter.previousBox = previousBox;
    splitter.nextBox = nextBox;
    splitters.add(splitter);

    if (!previousBox.id)
      previousBox.id = `resizable-box-${mResizableBoxCount++}`;
    if (!nextBox.id)
      nextBox.id = `resizable-box-${mResizableBoxCount++}`;

    mResizableBoxes.set(previousBox.id, previousBox);
    mResizableBoxes.set(nextBox.id, nextBox);
    splitter.addEventListener('mousedown', onMouseDown);
    splitter.addEventListener('mouseup', onMouseUp);
  }

  const alreadyApplied = new Set();
  for (const splitter of splitters) {
    for (const box of [splitter.previousBox, splitter.nextBox]) {
      if (alreadyApplied.has(box))
        continue;
      const size = mSizes[box.id];
      if (typeof size != 'number')
        continue;
      setBoxSize(box, size, splitter);
      alreadyApplied.add(box);
    }
  }
}

function findPreviousResizableBox(splitter) {
  let box = splitter.previousElementSibling;
  do {
    const style = window.getComputedStyle(box, null);
    if (style.display != 'none' &&
        style.visibility != 'collapse')
      break;;
    box = box.previousElementSibling;
  } while (box);
  return box;
}

function findNextResizableBox(splitter) {
  let box = splitter.nextElementSibling;
  do {
    const style = window.getComputedStyle(box, null);
    if (style.display != 'none' &&
        style.visibility != 'collapse')
      break;;
    box = box.nextElementSibling;
  } while (box);
  return box;
}

function getSizeProperty(box) {
  return box.classList.contains('row') ? 'height' : 'width';
}

function getEventCoordinate(event, splitter) {
  return splitter.classList.contains('row') ? event.screenY : event.screenX;
}

function getOffsetSize(box, splitter) {
  return splitter.classList.contains('row') ? box.offsetHeight : box.offsetWidth;
}

function setBoxSize(box, size, splitter) {
  box.style[getSizeProperty(splitter)] = size >= 0 ? `${size}px` : MIN_SIZE;
}

let mStartCoordinate;
let mStartPreviousSize;
let mStartNextSize;
let mResizingSplitter;

function onMouseDown(event) {
  mResizingSplitter = event.currentTarget;
  mStartCoordinate = getEventCoordinate(event, mResizingSplitter);
  mStartPreviousSize = getOffsetSize(mResizingSplitter.previousBox, mResizingSplitter);
  mStartNextSize = getOffsetSize(mResizingSplitter.nextBox, mResizingSplitter);
  mResizingSplitter.setCapture(false);
  window.addEventListener('mousemove', onResizing);
}

function resizeBoxesFor(splitter, event) {
  const delta = getEventCoordinate(event, splitter) - mStartCoordinate;
  setBoxSize(splitter.previousBox, mStartPreviousSize + delta, splitter);
  setBoxSize(splitter.nextBox, mStartNextSize - delta, splitter);
  for (const box of mResizableBoxes.values()) {
    if (box != splitter.previousBox &&
        box != splitter.nextBox) {
      setBoxSize(box, getOffsetSize(box, splitter), splitter);
    }
    mSizes[box.id] = getOffsetSize(box, splitter);
  }
  return { ...mSizes };
}

let mThrottledResize;

function onMouseUp(event) {
  if (mThrottledResize) {
    clearTimeout(mThrottledResize);
    mThrottledResize = null;
  }
  const resizeResult = resizeBoxesFor(mResizingSplitter, event);
  document.releaseCapture();
  window.removeEventListener('mousemove', onResizing);
  mStartCoordinate = null;
  mStartPreviousSize = null;
  mStartNextSize = null;
  mResizingSplitter = null;
  event.currentTarget.dispatchEvent(new CustomEvent(TYPE_RESIZED, {
    detail:     resizeResult,
    bubbles:    true,
    cancelable: false,
    composed:   true
  }));
}

function onResizing(event) {
  if (mThrottledResize)
    clearTimeout(mThrottledResize);
  mThrottledResize = setTimeout(() => {
    mThrottledResize = null;
    resizeBoxesFor(mResizingSplitter, event);
  }, 25);
}
