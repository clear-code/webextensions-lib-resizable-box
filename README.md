# webextensions-lib-resizable-box

A helper library to implement resizable boxes separated with a splitter.

## How to use

HTML:

```html
<div style="display: flex; flex-direction: row;">
  <div id="box1">...</div>
  <span class="splitter column"></span>
  <div id="box2">...</div>
</div>

<div style="display: flex; flex-direction: column;">
  <div id="box3">...</div>
  <hr class="splitter row">
  <div id="box4">...</div>
</div>
```

CSS:

```css
.splitter.column {
  border: none;
  cursor: col-resize;
  height: 100%;
  margin: 0;
  padding: 0;
  width: 0.25em;
}

.splitter.row {
  border: none;
  cursor: row-resize;
  height: 0.25em;
  margin: 0;
  padding: 0;
  width: 100%;
}
```

JavaScript:

```javascript
import * as ResizableBox from '/path/to/resizable-box.js';

var sizes = {
  // you can make this blank
  box1: 230,
  box2: 400,
  box3: 100,
  box4: 300
};

window.addEventListener('DOMContentLoaded', async () => {
  ResizableBox.init(sizes); // restore last state
  window.addEventListener(ResizableBox.TYPE_RESIZED, event => {
    // you can save the result to any storage.
    sizes = event.detail;
  });
}, { once: true });
```
